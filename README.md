# Tiny Connection Machine docs and example code

This repo contains helpful links and example code for the miniature Connection Machine by Trevor Flowers at [Transmutable.com](https://transmutable.com/).

This document also could be useful for anyone using an [AdaFruit QT Py / XIAO](https://www.adafruit.com/category/595) and the [LED matrices](https://www.adafruit.com/product/2974) driven by an [IS31FL3731 board](https://www.adafruit.com/product/2946).

The Connection Machines comes with these specific boards:
- [QT Py ESP32-S3](https://www.adafruit.com/product/5426)
- [LED matrix](https://www.adafruit.com/product/2974)
- [IS31FL3731 board](https://www.adafruit.com/product/2946).

Here are the excellent Adafruit learning guides for these boards:
- [QT Py](https://learn.adafruit.com/adafruit-qt-py-esp32-s3)
- [LED driver](https://learn.adafruit.com/i31fl3731-16x9-charliplexed-pwm-led-driver)

Hot tip: Open up those guides when you're programming as they demonstrate how to use All The Things in both CircuitPython and Arduino.

