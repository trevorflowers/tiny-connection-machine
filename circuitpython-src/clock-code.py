import os
import ssl
import rtc
import wifi
import time
import board
import busio
import random
import ipaddress
import socketpool

import adafruit_ntp
import adafruit_requests
import adafruit_framebuf
from adafruit_is31fl3731.matrix import Matrix as Display

TIME_ZONE_OFFSET = -7
LIGHT_LEVEL = 35

i2c = board.STEMMA_I2C()
displays = [
    Display(i2c, address=0x74),
    Display(i2c, address=0x75),
    Display(i2c, address=0x76),
    Display(i2c, address=0x77)
]

# This is a little confusing since the LED matrix is rotated
COLUMN_COUNT = displays[0].height
ROW_COUNT = displays[0].width
FONT_WIDTH = 5
FONT_HEIGHT = 8

frames = []
for i in range(len(displays)):
    buf = bytearray(32)  # 1 bytes for columns x 8 bits for rows = 32 bytes (9 bits is 2 bytes)
    frame_buffer = adafruit_framebuf.FrameBuffer(buf, FONT_WIDTH, FONT_HEIGHT, adafruit_framebuf.MVLSB)
    frames.append((frame_buffer, buf))

ROW_CHAR_OFFSET = -4
COLUMN_CHAR_OFFSET = 2

# Turn on each light by scanning across columns
def scan_lights():
    for col in range(COLUMN_COUNT):
        for row in range(ROW_COUNT):
            for display in displays:
                display.pixel(row, col, LIGHT_LEVEL)
        time.sleep(0.05)
        for row in range(ROW_COUNT):
            for display in displays:
                display.pixel(row, col, 0)
scan_lights()

# Set up networking
pool = None
requests = None
try:
    wifi.radio.connect(os.getenv('CIRCUITPY_WIFI_SSID'), os.getenv('CIRCUITPY_WIFI_PASSWORD'))
    pool = socketpool.SocketPool(wifi.radio)
    #print("My MAC addr:", [hex(i) for i in wifi.radio.mac_address])
    #print("My IP address is", wifi.radio.ipv4_address)
    requests = adafruit_requests.Session(pool, ssl.create_default_context())
except Exception as error:
    print("Error setting up networking", error)

real_time_clock = rtc.RTC()

def fetch_time():
    if not pool or not requests:
        return False
    try:
        real_time_clock.datetime = adafruit_ntp.NTP(pool, tz_offset=TIME_ZONE_OFFSET, socket_timeout=90).datetime
        has_fetched_time = True
        return True
    except Exception as error:
        print("Error fetching time:", error)
        return False

def draw_time(dt):
    time_string = "{0:02}{1:02}".format(dt.tm_hour, dt.tm_min)
    chars = [time_string[0], time_string[1], time_string[3], time_string[2]]

    for display_index in range(len(displays)):
        display = displays[display_index]
        frame_buffer = frames[display_index][0]
        buf = frames[display_index][1]
        frame_buffer.fill(0)
        frame_buffer.text(chars[display_index], 0, 0, color=1)

        display.fill(0)
        for col in range(FONT_WIDTH):
            bite = buf[col]
            for row in range(FONT_HEIGHT):
                bit = 1 << row & bite
                if bit:
                    level = LIGHT_LEVEL
                else:
                    level = 0
                if display_index <= 1:
                    display.pixel(ROW_CHAR_OFFSET + ROW_COUNT - row - 1, COLUMN_CHAR_OFFSET + col, level)
                else:
                    display.pixel(-ROW_CHAR_OFFSET + row, -COLUMN_CHAR_OFFSET + COLUMN_COUNT - col - 1, level)

last_hour = -1
last_minute = -1
has_fetched_time = False
while True:
    should_fetch = has_fetched_time is False
    should_draw = False
    if real_time_clock.datetime.tm_hour != last_hour:
        should_fetch = True # Fetch time once an hour to account for local clock drift
        should_draw = True
        last_hour = real_time_clock.datetime.tm_hour
    if real_time_clock.datetime.tm_min != last_minute:
        should_draw = True
        last_minute = real_time_clock.datetime.tm_min
    if should_fetch:
        has_fetched_time = has_fetched_time or fetch_time()
    if should_draw:
        draw_time(real_time_clock.datetime)
    time.sleep(1)
