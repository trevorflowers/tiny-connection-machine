import time
import board
import busio
import random

from adafruit_is31fl3731.matrix import Matrix as Display

i2c = board.STEMMA_I2C()
displays = [
    Display(i2c, address=0x74),
    Display(i2c, address=0x75),
    Display(i2c, address=0x76),
    Display(i2c, address=0x77)
]

LIGHT_LEVEL = 35
RATE_OF_CHANGE = 0.05
DEAD_COLUMN = 4
ROW_COUNT = displays[0].width # x
COLUMN_COUNT = displays[0].height # y

def scan_lights():
    for col in range(COLUMN_COUNT):
        if col is DEAD_COLUMN: continue
        for row in range(ROW_COUNT):
            for display in displays:
                display.pixel(row, col, LIGHT_LEVEL)
        time.sleep(0.1)
        for row in range(ROW_COUNT):
            for display in displays:
                display.pixel(row, col, 0)

def light_with_level(level, display):
    for col in range(COLUMN_COUNT):
        if col is DEAD_COLUMN: continue
        for row in range(ROW_COUNT):
            if random.randrange(0,10) < 8:
                continue
            if row >= level + 1:
                display.pixel(row, col, 0)
                continue
            if random.randrange(1, 10) < 4:
                display.pixel(row, col, 0)
                continue
            display.pixel(row, col, LIGHT_LEVEL)

scan_lights()

level = ROW_COUNT / 3
level_deltas = [3, 2, 1, 0, -1, -2, -3]
while True:
    level = level + random.choice(level_deltas)
    if level >= ROW_COUNT: level = ROW_COUNT - 4
    if level < 0: level = 2
    for display in displays:
        if random.randrange(0,2) == 0: continue
        light_with_level(level, display)
    time.sleep(random.random() * RATE_OF_CHANGE)

